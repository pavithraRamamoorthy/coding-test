const express = require('express');
const app = express();
const mongoose = require('mongoose');
mongoose.connect("mongodb://127.0.0.1/emloyee");

var db = mongoose.connection;
db.on('error', console.log("Db connection error"));
db.once('open', function() {
    console.log("Db connected successfully");
})

const createEmloyee = require('./routes/emloyee');

app.use('/emloyee',createEmloyee);

const port = 4000;
app.listen(port, function() {
    console.log(`server listening at ${port}`);
})

