import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {
  public employee!: FormGroup;

  constructor(private router:Router) {
    this.employee = new FormGroup({
      emloyeeName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      phoneNo: new FormControl(''),
    })
   }

  ngOnInit(): void {
  }

  public onSubmit(): void {
  console.log("***********",this.employee.value)
  }

}
