import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public employee!: FormGroup;

  constructor(private router:Router) {
    this.employee = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    })
   }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    if(this.employee?.valid){
      this.router.navigate(['empolyee'])
    }
  }

}
